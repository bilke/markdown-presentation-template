---
title: My title
subtitle: A subtitle
date: \formatdate{25}{01}{2023}
author: John Doe
keywords: [presentation]
theme: metropolis
themeoptions: sectionpage=progressbar,numbering=fraction,progressbar=frametitle
aspectratio: 169 # or 43
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
header-includes:
  - \include{setup}
  # Comment these out or set to an installed font on your system
  - \setsansfont[Scale=1.0,BoldFont={Fira Sans Medium}]{Fira Sans Light}
  - \setmonofont[Scale=0.7]{Fira Code}
codeBlockCaptions: True
colorlinks: true
---

# A slide

::: columns

:::: {.column width=50%}

## Sub-heading

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.

::::

:::: {.column width=50%}

Some image:

![](UFZ_Logo_RGB_EN.png)

- Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
- Aenean commodo ligula eget dolor. Aenean massa.
- Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

::::

:::

# Next slide

Some code:

```python
# This program prints Hello, world!

print('Hello, world!')
```

---

3 hyphens (`---`) create a subslide (no header).

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

# Links

- [Metropolis theme](https://packages.oth-regensburg.de/ctan/macros/latex/contrib/beamer-contrib/themes/metropolis/doc/metropolistheme.pdf)
- [Pandoc beamer tips](https://github.com/alexeygumirov/pandoc-beamer-how-to?cmdf=pandoc+mtheme)
