.PHONY: pdf
pdf:
	pandoc --pdf-engine=xelatex -t beamer slides.md -o presentation.pdf
