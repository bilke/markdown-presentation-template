FROM pandoc/latex
RUN tlmgr install beamertheme-metropolis pgfopts datetime fmtcount textpos authoraftertitle appendixnumberbeamer ragged2e latexmk
RUN apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/testing make font-fira-code font-fira ghostscript
