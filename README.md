# Markdown Presentation Template

A simple UFZ Beamer template for creating presentation slides with Markdown.

## Get started

- Fork this repo
- Edit `slides.md`
- Push back to your fork
- CI generates your presentation pdf

## Generate locally

### Requirements

See [Dockerfile](./Dockerfile):

- Pandoc
- Latex with `beamertheme-metropolis`-package
- [Fira and Fira Code fonts](https://github.com/tonsky/FiraCode) (optional)

### Generate PDF

```bash
make
# OR
pandoc --pdf-engine=xelatex -t beamer slides.md -o presentation.pdf
```
